#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BYTES_PER_LINE 12
#define INTELLISENSE_CHECK 1

/* transforms "Some-header example.h" to "_SOME_HEADER_EXAMPLE_H */
void path_to_define(char buf[], unsigned buf_size, char* file_path)
{
	/* cut off absolute path part */
	{
		char* temp = strrchr(file_path, '\\');

		if (temp)
			file_path = temp + 1;
	}

	/* add "_" at the beginning */
	buf[0] = '_';
	buf++;
	buf_size--;

	strcpy_s(buf, buf_size, file_path);

	unsigned size = strlen(file_path);

	for (unsigned i = 0; i < size; i++)
	{
		if (buf[i] == '.' ||
			buf[i] == '-' ||
			buf[i] == ' ')
			buf[i] = '_';
		else if (
			buf[i] >= 'a' &&
			buf[i] <= 'z')
			buf[i] += 'A' - 'a';
	}
}

int main(int argc, char **argv)
{
	char* file_path;
	char file_name[64];

	if (argc == 2)
	{
		file_path = argv[1];
	}
	else
	{
		printf("enter the file name:\n");
		scanf_s("%63s", file_name, sizeof(file_name));
		file_path = file_name;
	}

	FILE* fileptr;
	if (!fopen_s(&fileptr, file_path, "rb"))
	{
		fseek(fileptr, 0, SEEK_END);
		unsigned filelen = ftell(fileptr);
		rewind(fileptr);

		unsigned char* buf = (unsigned char*)malloc(filelen + 1);
		fread(buf, filelen, 1, fileptr);
		fclose(fileptr);

		char new_file_path[256];
		sprintf_s(new_file_path, sizeof(new_file_path), "%s.h", file_path);

		FILE* fp;
		if (!fopen_s(&fp, new_file_path, "w"))
		{
			char define_name[64];
			path_to_define(define_name, sizeof(define_name), new_file_path);

			fprintf_s(fp,
				"#ifndef %s\n#define %s\n\nunsigned char arr[] = \n{\n#ifdef __INTELLISENSE__\n0\n#else",
				define_name, define_name);

			for (unsigned i = 0; i < filelen; i++)
			{
				if (i % BYTES_PER_LINE == 0)
					fprintf_s(fp, "\n\t");

				fprintf_s(fp, "0x%02X, ", (unsigned)buf[i]);
			}

			fprintf_s(fp,
				"\n\n#endif /* __INTELLISENSE__ */\n};\n\n#endif /* %s */\n",
				define_name);

			fclose(fp);
		}
		else
		{
			printf("failed to create the file\n");
		}

		free(buf);
	}
	else
	{
		printf("failed to open the file\n");
	}

	return(0);
}
